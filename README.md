This is a full kafka connect example using two source connector taking data streams from Twitter and an elasticsearch sink connector
to insert tweets into elastic search.

The docker-compose file has been modified to use the images i created for landoop kafka cluster, elasticsearch and kibana.

To run cd to the directory of the project and run docker-compose up.

To access the kafka UI: http://192.168.99.100:3030/

To access Kibana UI: http://192.168.99.100:5601 -- the current index that can be found is "index1" for both source topics (POC).

The old elasticsearch interface that was used/commented currently is through the plugin:
http://192.168.99.100:9200/_plugin/dejavu/

